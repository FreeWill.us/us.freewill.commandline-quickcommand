﻿// Copyright (c) 1998-2019 freewill.us
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.
// or IPFS hash QmbUetW94JtdSdVdATC4b9ASxCaiRn94AyNpFrc6twsLKy
namespace us.FreeWill
{
    /// <summary>
    /// Some common printer control values.
    /// </summary>
    internal enum PrinterControlEnum
    {
        Bell           = 7,
        BackSpace      = 8,
        HorizontalTab  = 9,
        LF             = 10, // 0x0000000A
        LineFeed       = 10, // 0x0000000A
        VerticalTab    = 11, // 0x0000000B
        FF             = 12, // 0x0000000C
        FormFeed       = 12, // 0x0000000C
        CR             = 13, // 0x0000000D
        CarriageReturn = 13, // 0x0000000D
        Escape         = 27, // 0x0000001B
    }
}
