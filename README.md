# CommandLine
CommandLine is a .Net class library that makes writing command line interfaces simple.  Benefits include:

- Turning your CLI into an event-driven application.
- ZERO parsing needed on the application developer's effort.
- A HELP command is automatically added and automatically shows your help text for each command and each parameter in a nicely formatted mannor.
- An included EXIT command is provided, by default, which you can, of course, override with your own as well as choosing what word triggers the exit command.
- Can add CLI to any type of user interface app, including WinForms, Web apps, etc. (examples are included). 
- Command history, for free, with up and down arrows hit by the user.

## How to use:

1.  Instantiate a CommandLineType object.
2.  Instantiate 1 or more CommandType objects and add them to the CommandLineType object.
3.  Instantiate 0 or more ParameterType objects for each CommandType object and add them to the appropriate CommandType objects.
4.  Call the CommandLine object's Execute() method or it's ExecDosCommand(args) method.

## How to donate:
- BitCoin: bc1q5fdnm656j2rc200c85x2q6u0cu85v7hnwl3c7q

![BitCoin Donation](http://gateway.ipfs.io/ipfs/QmQx7ihUz9NoTt3X24bWJhUogDKLsfvG9wKpK4U6QrbeTg)

## Example usage:

```
using us.FreeWill.CommandLine;

namespace ComandLine.Example.Console
{
    class Program
    {
        private static CommandLineType _cl;

        static void Main(string[] args)
        {
            //Instantiate the command line object, giving the initial display prompt.
            _cl = new CommandLineType("Example", ">");
            
            //Instantiate a command object, along with some simple help text and a reference to the command's callback function.
            var testCommand = new CommandType(
                commandEventHandler: TestCommand_CommandFunction, 
                consoleEntry       : "test", 
                shortHelpString    : "Just a sample command.");

            //Add a parameter that can be used.
            var parm = new ParameterType(
                eventHandler    : Parm_ValidateFunction, 
                parmLabel       : "SomeParm", 
                equationString  : "=", //What you expecte between the parm name and value (SomeParm = Hello).
                defaultUserValue: "Hello", 
                shortHelpString : "A sample command parameter that defaults to value \"Hello\".");

            //Add a parameter that can be used.
            var parm2 = new ParameterType(
                eventHandler    : Parm_ValidateFunction, 
                parmLabel       : "SomethingElse", 
                equationString  : "=", //What you expecte between the parm name and value (SomethingElse = World).
                defaultUserValue: "World", 
                shortHelpString : "A sample command parameter that defaults to value \"World\".");

            testCommand.ParameterList.Add(parm);
            testCommand.ParameterList.Add(parm2);

            //Add the command to the command line object so it can process it when users enter it.
            _cl.AddCommand(testCommand);
            
            //Tell it which of your commands exits the program (you can add extra functionality to the exit process here).
            //Comment out the next line and you'll get the custom exit command instead, along with its default help.
            _cl.SetExitCommand(new CommandType((NotifyEventDelegate)null, "Quit", "My custom exit command."));

            if (args.Length > 0)
            {
                //If launched from the OS console (or anywhere else) with parms passed in,
                //execute the one command and exit.
                _cl.ExecDosCommand(args);
            }
            else
            {
                DisplayWelcome(_cl);

                //Start up the command line.  It won't stop until someone enters the text you provided
                //  for the exit command OR if you provided no exit command, when the user types the
                //  text for the default exit command, which is "Exit".
                _cl.Execute();
            }
        }

        //Automatically called when parm or parm2 are used.
        //Their values are validated and processed here.
        private static bool Parm_ValidateFunction(object sender)
        {
            var parm = (ParameterType) sender;
            _cl.OutputStream.WriteLine($"Parameter recognized \"{parm.ParamText}\" with value \"{parm.UserValue}\".");

            //Do something special here.
            if (parm.UserValue.ToLower() == "fail")
                return false; //We don't like the value sent.

            //We like the values provided.  Return true.
            return true;
        }

        /// <summary>
        /// The event handler for the Test command.  This will be called automatically when the user enters the "test" command.
        /// You write the code to handle the "Test" command here.
        /// </summary>
        /// <param name="sender">The sending CommandType object.</param>
        /// <returns>You return true or false depending on whether you determined this succeeded or failed.</returns>
        private static bool TestCommand_CommandFunction(object sender)
        {
            var command = (CommandType) sender;

            var cl = command.GetOwnerCommandLine();
            //We don't write to Console. We write to the command line object's OutputStream.
            //  We can redirect THAT to Console, if we like, or anywhere else.
            cl.OutputStream.WriteLine($"You entered [{command.ConsoleEntry}]");
            
            if (command.ConsoleEntry.Contains("fail"))
                return false;

            return true;
        }

        private static void DisplayWelcome(CommandLineType cl)
        {
            //Write a little intro for the users on startup...
            //Notice your CommandLineType object has an output stream.  You should always use this
            //instead of Console.WriteLine because you can change OutputStream to any stream you want.
            //CL defaults to the System.Console.Out TextWriter, but you can change it ot anything you want.

            cl.OutputStream.WriteLine("*******************************************************************");
            cl.OutputStream.WriteLine("*                                                                 *");
            cl.OutputStream.WriteLine("*  #     #           ##                                       ##  *");
            cl.OutputStream.WriteLine("*  #     #            #                                       ##  *");
            cl.OutputStream.WriteLine("*  #     #            #                                       ##  *");
            cl.OutputStream.WriteLine("*  #     #            #                                       ##  *");
            cl.OutputStream.WriteLine("*  #  #  #  #####     #     #####   #####  ### ##   #####     ##  *");
            cl.OutputStream.WriteLine("*  # ### # #     #    #    #     # #     # ## #  # #     #    ##  *");
            cl.OutputStream.WriteLine("*  ### ### #######    #    #       #     # #  #  # #######    ##  *");
            cl.OutputStream.WriteLine("*  ##   ## #          #    #     # #     # #  #  # #              *");
            cl.OutputStream.WriteLine("*  #     #  #####    ###    #####   #####  #     #  #####     ##  *");
            cl.OutputStream.WriteLine("*                                                                 *");
            cl.OutputStream.WriteLine("*******************************************************************\r\n");

            cl.OutputStream.WriteLine("Welcome to the CommandLine sample Console application.");
            cl.OutputStream.WriteLine("Type \"Help\".");
        }
    }
}
```

The above program produces the following output when the user types "HELP" at the command line.  The default help facility takes the help text provided by the app developer and displays it in a cleanly formatted fasion:

```
*******************************************************************
*                                                                 *
*  #     #           ##                                       ##  *
*  #     #            #                                       ##  *
*  #     #            #                                       ##  *
*  #     #            #                                       ##  *
*  #  #  #  #####     #     #####   #####  ### ##   #####     ##  *
*  # ### # #     #    #    #     # #     # ## #  # #     #    ##  *
*  ### ### #######    #    #       #     # #  #  # #######    ##  *
*  ##   ## #          #    #     # #     # #  #  # #              *
*  #     #  #####    ###    #####   #####  #     #  #####     ##  *
*                                                                 *
*******************************************************************

Welcome to the CommandLine sample Console application.
Type "Help".

Example>help


    Commands    Help Summary
    ----------- ----------------------------------------------------------------
    HELP        Displays all commands and a short description of what they do.

    TEST        Just a sample command.

                    SOMEPARM     [=] A sample command parameter that defaults to value "Hello".
                    SOMETHINGELSE[=] A sample command parameter that defaults to value "World".

    QUIT        My custom exit command.

    Usage: command parm1 = setting1 parm2 = setting2


Example>
```

User enters the "test" command:

```
Example>test

You entered [test]
```

User enters the "test" command with the "someparm" parameter:

```
Example>test someparm = hello

Parameter recognized "SOMEPARM" with value "hello".
You entered [test]
```

User enters the "test" command with the "somethingelse" parameter:

```
Example>test somethingelse = world!

Parameter recognized "SOMETHINGELSE" with value "world!".
You entered [test]
```

User enters both parameters with the "test" command:

```
Example>test someparm = hello somethingelse = world!

Parameter recognized "SOMEPARM" with value "hello".
Parameter recognized "SOMETHINGELSE" with value "world!".
You entered [test]
```

Parameters can be provided in any order.  Here's the user entering the same two parameters from the "test" command in a different order:

```
Example>test somethingelse = world! someparm = hello

Parameter recognized "SOMETHINGELSE" with value "world!".
Parameter recognized "SOMEPARM" with value "hello".
You entered [test]
```

The CommandLine library handles unrecognized user commands:

```
Example>not a command


    Unrecognized Command.  Valid Commands are:
    ------------------------------------------

    HELP        Displays all commands and a short description of what they do.

    TEST        Just a sample command.

                    SOMEPARM     [=] A sample command parameter that defaults to value "Hello".
                    SOMETHINGELSE[=] A sample command parameter that defaults to value "World".

    QUIT        My custom exit command.

    Usage: command parm1 = setting1 parm2 = setting2
```

If you're using the CommandLine library in a Console app, you get command history for free too by hitting the up or down arrows.

## History
I created this out of need back in 1998, when I was writing an unusually large amount of command line programs.  It was originally written in C++, then downgraded to C (stupid work requirements!), then upgraded to C# around 2003.  It was last touched around 2007.  Sometime between then and 2018, I lost the source code, but still had the latest binary.  Fortunately, .Net code can be decompiled back into C# code.  From that, I cleaned it up and modernized it.  I also added plenty of comments and documentation, sample applications, and a Nuget package, polishing it up before publishing it in 2019 (21 years after it's original creation).