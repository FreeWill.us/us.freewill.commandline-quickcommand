﻿// Copyright (c) 1998-2019 freewill.us
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.
// or IPFS hash QmbUetW94JtdSdVdATC4b9ASxCaiRn94AyNpFrc6twsLKy
using System;
using NUnit.Framework;
using us.FreeWill.CommandLine;

namespace CommandLine.Tests
{
    [TestFixture]
    public class CommandLineTests
    {
        private bool ParameterEventHandler_Test_ParmEventHandlerFired = false;
        private string ParameterEventHandler_Test_Value = null;

        [Test]
        public void ParameterEventHandler_Test()
        {
            var cl      = new CommandLineType();
            var command = new CommandType("nunit");
            var parm    = new ParameterType(Parm_ValidateFunction, "testparm", "=", "default", "nunit short help");
            command.ParameterList.Add(parm);
            cl.AddCommand(command);

            this.ParameterEventHandler_Test_ParmEventHandlerFired = false;
            this.ParameterEventHandler_Test_Value = null;
            var testParmValue = "something";
            cl.ExecCommand($"nunit testparm = {testParmValue}");

            Assert.True(this.ParameterEventHandler_Test_ParmEventHandlerFired, "Parameter event handler failed to fire!");

            Console.WriteLine("1st of 2 tests passed:  Parameter event handler fired!");

            Assert.AreEqual(testParmValue, this.ParameterEventHandler_Test_Value, $"Parm did NOT receive expected value of [{testParmValue}], but was {this.ParameterEventHandler_Test_Value}");

            Console.WriteLine("2nd of 2 tests passed:  User parameter value received in paramter event handler!");
        }

        private bool Parm_ValidateFunction(object sender)
        {
            var parm = (ParameterType)sender;
            this.ParameterEventHandler_Test_ParmEventHandlerFired = true;
            this.ParameterEventHandler_Test_Value = parm.UserValue;
            return true;
        }
    }
}
