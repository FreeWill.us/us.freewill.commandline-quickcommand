﻿// Copyright (c) 1998-2019 freewill.us
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.
// or IPFS hash QmbUetW94JtdSdVdATC4b9ASxCaiRn94AyNpFrc6twsLKy
namespace us.FreeWill
{
    /// <summary>
    /// Some special keys that users could hit.
    /// </summary>
    public enum SpecialKeysEnum
    {
        None      = -1,
        Function  = 0,
        BackSpace = 8,
        Tab       = 9,
        PageBreak = 12, // 0x0000000C
        CR        = 13, // 0x0000000D
        Enter     = 13, // 0x0000000D
        Esc       = 27, // 0x0000001B
        F1        = 59, // 0x0000003B
        F2        = 60, // 0x0000003C
        F3        = 61, // 0x0000003D
        F4        = 62, // 0x0000003E
        F5        = 63, // 0x0000003F
        F6        = 64, // 0x00000040
        F7        = 65, // 0x00000041
        F8        = 66, // 0x00000042
        F9        = 67, // 0x00000043
        F10       = 68, // 0x00000044
        Home      = 71, // 0x00000047
        Up        = 72, // 0x00000048
        PageUp    = 73, // 0x00000049
        Left      = 75, // 0x0000004B
        Right     = 77, // 0x0000004D
        End       = 79, // 0x0000004F
        Down      = 80, // 0x00000050
        PageDown  = 81, // 0x00000051
        Insert    = 82, // 0x00000052
        Delete    = 83, // 0x00000053
        F11       = 133, // 0x00000085
        F12       = 134, // 0x00000086
        Modifier  = 224, // 0x000000E0
    }
}
