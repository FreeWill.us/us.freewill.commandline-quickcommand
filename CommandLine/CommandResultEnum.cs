﻿// Copyright (c) 1998-2019 freewill.us
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.
// or IPFS hash QmbUetW94JtdSdVdATC4b9ASxCaiRn94AyNpFrc6twsLKy
namespace us.FreeWill.CommandLine
{
    /// <summary>
    /// Enumerated values that could returned by commands.
    /// </summary>
    public enum CommandResultEnum
    {
        Success,
        Fail,
        Exit,
    }
}
