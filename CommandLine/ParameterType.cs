﻿// Copyright (c) 1998-2019 freewill.us
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.
// or IPFS hash QmbUetW94JtdSdVdATC4b9ASxCaiRn94AyNpFrc6twsLKy
using System.Collections.Generic;

namespace us.FreeWill.CommandLine
{
    /// <summary>
    /// Every parameter for a command will need a ParamterType object.
    /// </summary>
    public class ParameterType
    {
        /// <summary>
        /// Short text to display in the automated help command for this parameter.
        /// </summary>
        public List<string> ShortHelp    = new List<string>();

        /// <summary>
        /// Long text to display in the automated help command for this parameter.
        /// </summary>
        public List<string> LongHelp     = new List<string>();

        /// <summary>
        /// List of strings that will be accepted as equals text.
        /// For example, a command like TEST MYPARM = HELLO, the "=" is the ValidEquate.
        /// You can accept more than one character or string as a valid equals string
        /// such as this:  TEST MYPARM := HELLO, the ":=" string is the ValidEquate.
        /// You can have it accept more than just one at any time.
        /// </summary>
        public List<string> ValidEquates = new List<string>();

        /// <summary>
        /// The paramter name expected from the user.
        /// </summary>
        public string       ParamText;

        /// <summary>
        /// The equals string the user provided.
        /// </summary>
        public string       UserEquate;

        /// <summary>
        /// The value for the parameter the user provided.  This is what follows the equals character.
        /// </summary>
        public string       UserValue;

        /// <summary>
        /// Boolean enforcing whether a command can be accepted without this paramter.
        /// </summary>
        public bool         Required;

        public ParameterType()
        {
            this.Init();
        }

        public ParameterType(NotifyEventDelegate eventHandler, string parmLabel, string equationString, string defaultUserValue, string shortHelpString)
        {
            this.Init();
            this.SetValidateFunction(eventHandler);
            this.ParamText = parmLabel;
            this.UserValue = defaultUserValue;
            this.ValidEquates.Add(equationString);
            this.ShortHelp.Add(shortHelpString);
        }

        public ParameterType(NotifyEventDelegate eventHandler, string parmLabel, List<string> shortHelp, List<string> longHelp)
        {
            this.Init();
            this.SetValidateFunction(eventHandler);
            this.ParamText = parmLabel;

            this.ShortHelp.AddRange(shortHelp);
            this.LongHelp.AddRange(longHelp);
        }

        /// <summary>
        /// Manually assign your own paramter validation event handler.
        /// It will automatically be called when users enter this parameter
        /// so you can hook into validating what the user enters.
        /// </summary>
        /// <param name="validateEventHandler">Your event handler.</param>
        /// <returns>true if paramter is validated.</returns>
        public bool SetValidateFunction(NotifyEventDelegate validateEventHandler)
        {
            this.ValidateFunction = validateEventHandler;
            return validateEventHandler != null;
        }

        /// <summary>
        /// Perform a vlidation on the user entered paramter value.
        /// </summary>
        /// <returns>true if the user entered value is validated.</returns>
        public bool Validate()
        {
            if (this.ValidateFunction != null)
                return this.ValidateFunction((object)this);
            return true;
        }

        /// <summary>
        /// The validation function event.
        /// </summary>
        public event NotifyEventDelegate ValidateFunction;

        /// <summary>
        /// Initialize the paramter object.
        /// </summary>
        protected void Init()
        {
            this.ValidateFunction = (NotifyEventDelegate)null;
            this.Required = false;
        }
    }
}
