﻿// Copyright (c) 1998-2019 freewill.us
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.
// or IPFS hash QmbUetW94JtdSdVdATC4b9ASxCaiRn94AyNpFrc6twsLKy
using System.Collections.Generic;
using System.Linq;

namespace us.FreeWill.CommandLine
{
    /// <summary>
    /// Every command you need in your CLI should have its own CommandType object.
    /// </summary>
    public class CommandType
    {
        /// <summary>
        /// List of parameters this command accepts.
        /// </summary>
        public List<ParameterType> ParameterList = new List<ParameterType>();

        /// <summary>
        /// A short list of text strings for this command that will be displayed when the user asks for help.
        /// </summary>
        public List<string> ShortHelp = new List<string>();

        /// <summary>
        /// A long list of text strings for this command that will be displayed when the user asks for help.
        /// </summary>
        public List<string> LongHelp = new List<string>();

        /// <summary>
        /// The text that the user typed.
        /// </summary>
        public string ConsoleEntry;

        /// <summary>
        /// A list of every token entered, minus the command text.
        /// Essentially, ConsoleEntry.Split(' '), minus the first element.
        /// </summary>
        public List<string> UserTokens { get; set; } = new List<string>();

        /// <summary>
        /// The CommandLineType object that this command belongs to.
        /// </summary>
        protected CommandLineType OwnerCommandLine;

        ///// <summary>
        ///// List of paramters that the user entered.
        ///// </summary>
        //protected List<string> UserParams;

        public CommandType()
        {
            this.OwnerCommandLine = null;
            this.CommandEventHandler = null;
        }

        public CommandType(string consoleEntry)
        {
            this.CommandEventHandler = null;
            this.ConsoleEntry = consoleEntry;
        }

        public CommandType(string consoleEntry, string shortHelpString)
        {
            this.ConsoleEntry = consoleEntry;
            this.ShortHelp.Add(shortHelpString);
            this.CommandEventHandler = null;
        }

        public CommandType(string consoleEntry, List<string> shortHelp)
        {
            this.ConsoleEntry = consoleEntry;
            this.ShortHelp.AddRange(shortHelp);
            this.CommandEventHandler = null;
        }

        public CommandType(string consoleEntry, List<string> shortHelp, List<string> longHelp)
        {
            this.ConsoleEntry = consoleEntry;
            this.ShortHelp.AddRange(ShortHelp);
            this.LongHelp.AddRange(longHelp);
            this.CommandEventHandler = null;
        }

        public CommandType(NotifyEventDelegate commandEventHandler, string consoleEntry)
        {
            this.CommandEventHandler = commandEventHandler;
            this.ConsoleEntry = consoleEntry;
        }

        public CommandType(NotifyEventDelegate commandEventHandler, string consoleEntry, string shortHelpString)
        {
            this.ConsoleEntry = consoleEntry;
            this.ShortHelp.Add(shortHelpString);
            this.CommandEventHandler = commandEventHandler;
        }

        public CommandType(NotifyEventDelegate commandEventHandler, string consoleEntry, List<string> shortHelp, List<string> longHelp)
        {
            this.ConsoleEntry = consoleEntry;
            this.ShortHelp.AddRange(shortHelp);
            this.LongHelp.AddRange(longHelp);
            this.CommandEventHandler = commandEventHandler;
        }

        /// <summary>
        /// Executes the developer provided event handler method for this CommandType object.
        /// </summary>
        /// <returns>boolean value that the application developer returns from their event handerl.</returns>
        public virtual bool Execute()
        {
            if (this.CommandEventHandler == null)
                return false;
            return this.CommandEventHandler(this);
        }

        public event NotifyEventDelegate CommandEventHandler;

        /// <summary>
        /// After user command is parsed, all words found are passed here as a string list.
        /// </summary>
        /// <param name="parameters">list of paramter words found in the user entered command.</param>
        /// <returns></returns>
        public bool SetUserParams(List<string> parameters)
        {
            int index1 = 1;
            while (index1 + 2 < parameters.Count)
            {
                bool isParmMatched = false;

                foreach(var parameter in this.ParameterList)
                {
                    parameters[index1] = parameters[index1].ToUpper();
                    parameter.ParamText = parameter.ParamText.ToUpper();
                    if (parameter.ParamText == parameters[index1])
                    {
                        isParmMatched = true;
                        if (parameter.ValidEquates.Any(e => e == parameters[index1 + 1]))
                        {
                            string userSetting = parameter.UserValue;
                            parameter.UserValue = parameters[index1 + 2];
                            if (parameter.Validate())
                            {
                                parameter.UserValue = parameters[index1 + 2];
                            }
                            else
                            {
                                parameter.UserValue = userSetting;
                                return false;
                            }
                        }
                        else
                        {
                            this.OwnerCommandLine.OutputStream.WriteLine("Syntax error: \"" + parameters[index1 + 1] + "\"");
                            return false;
                        }
                    }
                }

                if (!isParmMatched)
                    return false;

                index1 += 3;
            }
            return true;
        }

        /// <summary>
        /// Retrieve a full ParameterType object by it's ParamText property value, if it exists.
        /// </summary>
        /// <param name="parameterName">The text of the paramter object to find.</param>
        /// <returns>A ParamterType object found or null.</returns>
        public ParameterType GetParameterByName(string parameterName)
        {
            return this.ParameterList.FirstOrDefault(p => p.ParamText.ToUpper() == parameterName.ToUpper());
        }

        /// <summary>
        /// Assign your command object to a specific CommandLinType object.
        /// </summary>
        /// <param name="owner">The owning CommandLineType object.</param>
        /// <returns>true</returns>
        public bool SetOwnerCommandLine(CommandLineType owner)
        {
            this.OwnerCommandLine = owner;
            return true;
        }

        /// <summary>
        /// Manually add your own event handler that will be called when a user types this command.
        /// </summary>
        /// <param name="commandEventHandler">Your own event handler reference.</param>
        /// <returns>true if successful, false if it's null.</returns>
        public bool SetFunction(NotifyEventDelegate commandEventHandler)
        {
            if (commandEventHandler == null)
                return false;
            this.CommandEventHandler = commandEventHandler;
            return true;
        }

        /// <summary>
        /// Retrieve a reference to the CommandLineType object that this command belongs to.
        /// </summary>
        /// <returns></returns>
        public CommandLineType GetOwnerCommandLine()
        {
            return this.OwnerCommandLine;
        }
    }
}
