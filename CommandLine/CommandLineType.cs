﻿// Copyright (c) 1998-2019 freewill.us
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.
// or IPFS hash QmbUetW94JtdSdVdATC4b9ASxCaiRn94AyNpFrc6twsLKy
using System;
using System.Collections.Generic;
using System.IO;

namespace us.FreeWill.CommandLine
{
    /// <summary>
    /// Class that represents the Command Line object.  All commands are added to this object.  This object parses user commands and routes execution to the appropriate command object event handlers.
    /// </summary>
    public class CommandLineType
    {
        public string            Prompt;
        public string            Name;
        public List<CommandType> CommandList = new List<CommandType>();

        protected List<string> CommandStringHistory = new List<string>();
        protected CommandType  HelpCommand;
        protected CommandType  ExitCommand;
        protected string       CommandEntered;

        private TextWriter _originalOutputStream = (TextWriter)new StringWriter();
        
        #region Constructors...

        public CommandLineType()
        {
            this.SetDefaults();
            this.Prompt = ":";
        }

        /// <summary>
        /// Constructor that takes a command name.
        /// </summary>
        /// <param name="name">The text to display on the command prompt.</param>
        public CommandLineType(string name)
        {
            this.SetDefaultsPlusName(name);
            this.Prompt = name + ":";
        }

        /// <summary>
        /// Constructor that takes a command name and a display prompt.
        /// </summary>
        /// <param name="name">The text to display on the command prompt.</param>
        /// <param name="prompt">The prompt to display just before the flashing insert cursor.</param>
        public CommandLineType(string name, string prompt)
        {
            SetDefaultsPlusNameAndPrompt(name, prompt);
        }

        /// <summary>
        /// Constructor that takes a command name, a display prompt, and an exit command object.
        /// </summary>
        /// <param name="name">The text to display on the command prompt.</param>
        /// <param name="prompt">The prompt to display just before the flashing insert cursor.</param>
        /// <param name="exitCommand">A developer provided exit command to override the built-in exit command.</param>
        public CommandLineType(string name, string prompt, CommandType exitCommand)
        {
            this.SetDefaultsPlusNameAndPrompt(name, prompt);
            this.SetExitCommand(exitCommand);
        }

        #endregion


        #region Setting defaults...

        private void SetDefaults()
        {
            this.HelpCommand = null;
            this.ExitCommand = null;
            this.SetDefaultExitCommand();
        }

        private void SetDefaultsPlusName(string name)
        {
            this.SetDefaults();
            this.Name = name;
        }

        private void SetDefaultsPlusNameAndPrompt(string name, string prompt)
        {
            this.SetDefaultsPlusName(name);
            this.Prompt = name + prompt;
        }

        private void SetDefaultExitCommand()
        {
            this.SetExitCommand(new CommandType(DefaultExitCommand_CommandFunction, "Exit", "Exits this application."));
        }

        private bool DefaultExitCommand_CommandFunction(object sender)
        {
            return true;
        }

        #endregion


        //TODO:  Why is this here?
        protected bool AddHistory(CommandType command)
        {
            return true;
        }

        ~CommandLineType()
        {
        }

        public CommandResultEnum GetCommand()
        {
            this._outputStream.WriteLine("");
            this._outputStream.Write(this.Prompt);
            this.CommandEntered = Console.ReadLine();
            this._outputStream.WriteLine("");
            if (this.CommandEntered != null)
                return this.ExecCommand(this.CommandEntered);
            return CommandResultEnum.Fail;
        }

        /// <summary>
        /// Tage arguments passed in from the OS console command and process them.
        /// </summary>
        /// <param name="arguments">The list of arguments passed in from the OS console.</param>
        /// <returns></returns>
        public CommandResultEnum ExecDosCommand(string[] arguments)
        {
            var fullArgumentText = "";
            int totalLength = 0;

            foreach (var argument in arguments)
                totalLength += argument.Length;

            if (totalLength > 0)
            {
                foreach (var argument in arguments)
                    fullArgumentText+= $"{argument} ";

                this._outputStream.WriteLine($"Processing command = \"{fullArgumentText.Trim()}\".");
            }

            var commandResult = this.ExecCommand(fullArgumentText);
            if (commandResult == CommandResultEnum.Fail)
            {
                this._outputStream.WriteLine("\nUnrecognized Command.  Valid Commands are:\n------------------------------------------\n");
                this.PrintCommands();
            }
            return commandResult;
        }

        /// <summary>
        /// Allows processing of a command manually from your code without calling the CommandLineType.Execute() method.
        /// </summary>
        /// <param name="command">The full text from the user's command entry.</param>
        /// <returns>A CommandResultEnum value.</returns>
        /// <remarks>Good for use outside of a console app where you can give the running thread completely over to the command processor which just sits there waiting for keystrokes.</remarks>
        public CommandResultEnum ExecCommand(string command)
        {
            if (string.IsNullOrEmpty(command))
                return CommandResultEnum.Success;

            var newParams = new List<string>();

            this.CommandStringHistory.Add(command);

            string newParameter          = "";
            int newCommandExtraIndex     = 0;
            int newCommandIndex          = 0;
            int newCommandOriginalLength = command.Length;
            int newCommandExtraIndex2;

            //I lost my original source code around 2007 and in 2018, I used my latest binary to decompile back into source.
            //This for loop is a result of that.  Leaving it for now, in spite of this being CRAZY looking code!
            for (; newCommandIndex < newCommandOriginalLength; newCommandExtraIndex = newCommandIndex = newCommandExtraIndex2 + 1)
            {
                //Scan past spaces in user command to find next token.
                while (newCommandExtraIndex < newCommandOriginalLength && command[newCommandExtraIndex] == ' ')
                    ++newCommandExtraIndex;

                bool isInsideQuote;
                if (command[newCommandExtraIndex] == '"')
                {
                    isInsideQuote = true;
                    ++newCommandExtraIndex;
                    newCommandExtraIndex2 = newCommandExtraIndex;
                    //scan til we find the end of the quote.
                    while (newCommandExtraIndex2 < newCommandOriginalLength && command[newCommandExtraIndex2] != '"')
                        ++newCommandExtraIndex2;
                }
                else
                {
                    isInsideQuote = false;
                    newCommandExtraIndex2 = newCommandExtraIndex;
                    //scan past spaces...
                    while (newCommandExtraIndex2 < newCommandOriginalLength && command[newCommandExtraIndex2] != ' ')
                        ++newCommandExtraIndex2;
                }

                int index3 = newCommandExtraIndex;
                for (int index4 = 0; index4 < newCommandExtraIndex2 - newCommandExtraIndex; ++index4)
                {
                    //build the next token...
                    newParameter += (object)command[index3];
                    ++index3;
                }

                newParams.Add(newParameter);
                newParameter = "";
                if (isInsideQuote)
                    ++newCommandExtraIndex2;
            }

            int commandIndex = 0;

            //TODO:  Bug:  this.CommandList[commandIndex].ConsoleEntry is null here.
            this.CommandList[commandIndex].ConsoleEntry = this.CommandList[commandIndex].ConsoleEntry?.Trim(); //TODO: Does this do anything?
            newParams[0] = newParams[0].Trim();
            while (commandIndex < this.CommandList.Count && this.CommandList[commandIndex].ConsoleEntry?.ToUpper() != newParams[0].ToUpper())
            {
                ++commandIndex;
                if (commandIndex < this.CommandList.Count)
                    this.CommandList[commandIndex].ConsoleEntry = this.CommandList[commandIndex].ConsoleEntry.Trim();
            }

            if (commandIndex < this.CommandList.Count)
            {
                this.CommandList[commandIndex].UserTokens.Clear();
                for (int x = 1; x < newParams.Count; x++)
                {
                    this.CommandList[commandIndex].UserTokens.Add(newParams[x]);
                }
                if (this.CommandList[commandIndex].SetUserParams(newParams))
                    this.CommandList[commandIndex].Execute();
            }
            else
            {
                if (this.ExitCommand != null && this.ExitCommand.ConsoleEntry.ToUpper() == command.ToUpper())
                    return CommandResultEnum.Exit;
                if (this.HelpCommand != null)
                {
                    if (this.HelpCommand.ConsoleEntry != command)
                        return CommandResultEnum.Fail;
                    this.HelpCommand.Execute();
                }
                else
                {
                    if (command.ToUpper() != "HELP")
                        return CommandResultEnum.Fail;
                    this._outputStream.WriteLine("\n    Commands    Help Summary");
                    this._outputStream.WriteLine("    ----------- ----------------------------------------------------------------");
                    this.PrintCommands();
                }
            }

            return CommandResultEnum.Success;
        }

        /// <summary>
        /// Removes a command from the command list.
        /// </summary>
        /// <param name="command">The command to remove.</param>
        /// <returns>True if it was in the list to remove, false otherwise.</returns>
        public bool RemoveCommand(CommandType command)
        {
            bool canBeRemoved = this.CommandList.Contains(command);
            if (canBeRemoved)
                this.CommandList.Remove(command);

            return canBeRemoved;
        }

        /// <summary>
        /// Add a command to the command line, auto-setting it's owner to this.
        /// </summary>
        /// <param name="command">The command to add to the list of commands.</param>
        /// <returns>Returns true if successfully added, false if it's already there.</returns>
        public bool AddCommand(CommandType command)
        {
            command.SetOwnerCommandLine(this);
            if (!this.CommandList.Contains(command))
            {
                this.CommandList.Add(command);
                return true;
            }
            return false;
        }

        /// <summary>
        /// For a Console app, this initiates the command line.  Call this and it handles everything until the user enters the exit command.
        /// </summary>
        /// <returns>true, for now.  Reserved for future enhancements.</returns>
        public bool Execute()
        {
            var commandResult = CommandResultEnum.Success;
            while (commandResult != CommandResultEnum.Exit)
            {
                commandResult = this.GetCommand();
                if (commandResult == CommandResultEnum.Fail)
                {
                    this._outputStream.WriteLine("\n    Unrecognized Command.  Valid Commands are:\n    ------------------------------------------\n");
                    this.PrintCommands();
                }
            }
            return true;
        }

        /// <summary>
        /// Overrites the built in help command with your own.
        /// </summary>
        /// <param name="command">The command you want executed when the user enters "help" (or whatever you've replaced "help" with in the command object).</param>
        /// <returns>true.</returns>
        public bool SetHelpCommand(CommandType command)
        {
            this.HelpCommand = command;
            this.HelpCommand.SetOwnerCommandLine(this);
            return true;
        }

        /// <summary>
        /// Overrides the built in Exit command with your own.
        /// </summary>
        /// <param name="command">The command you want executed when the user types "exit" (or whatever you've replaced "exit" with in the command object).</param>
        /// <returns>true.</returns>
        public bool SetExitCommand(CommandType command)
        {
            this.ExitCommand = command;
            this.ExitCommand.SetOwnerCommandLine(this);
            return true;
        }

        /// <summary>
        /// Pump the formatted help text from all the commands and parameters through the output TextWriter.
        /// </summary>
        /// <returns>true.</returns>
        public bool PrintCommands()
        {
            if (this.HelpCommand != null)
            {
                this._outputStream.Write("    " + this.HelpCommand.ConsoleEntry.ToUpper());
                if (this.HelpCommand.ShortHelp.Count > 0)
                {
                    for (int index = 1; index < 13 - this.HelpCommand.ConsoleEntry.Length; ++index)
                        this._outputStream.Write(" ");

                    this._outputStream.WriteLine((object)this.HelpCommand.ShortHelp);

                    for (int index = 1; index <= this.HelpCommand.ShortHelp.Count - 1; ++index)
                        this._outputStream.WriteLine("                    " + this.HelpCommand.ShortHelp[index]);
                }
                this._outputStream.WriteLine();
            }
            else
                this._outputStream.WriteLine("    HELP        Displays all commands and a short description of what they do.\n");

            foreach(var command in this.CommandList)
            {
                if (command.ConsoleEntry != null)
                    this._outputStream.Write("    " + command.ConsoleEntry.ToUpper());

                if (command.ShortHelp.Count > 0)
                {
                    if (command.ConsoleEntry != null)
                    {
                        //TODO:  Confirm this change works.
                        //for (int index2 = 1; index2 < 13 - command.ConsoleEntry.Length; ++index2)
                        //    this._outputStream.Write(" ");
                        this._outputStream.Write(new string(' ', 12 - command.ConsoleEntry.Length));
                    }

                    this._outputStream.WriteLine(command.ShortHelp[0]);

                    for (int index2 = 1; index2 < command.ShortHelp.Count; ++index2)
                        this._outputStream.WriteLine("                    " + command.ShortHelp[index2]);

                    if (command.ParameterList.Count > 0)
                        this._outputStream.WriteLine("");

                    foreach(var parameter in command.ParameterList)
                    {
                        this._outputStream.Write("                    " + parameter.ParamText.ToUpper());

                        //TODO:  Confirm this change works.
                        //for (int length = parameter.ParamText.Length; length < 13; ++length)
                        //    this._outputStream.Write(" ");
                        this._outputStream.Write(new string(' ', 13 - parameter.ParamText.Length));

                        if (parameter.ValidEquates.Count > 0)
                        {
                            this._outputStream.Write("[");
                            for (int index3 = 0; index3 < parameter.ValidEquates.Count; ++index3)
                            {
                                this._outputStream.Write(parameter.ValidEquates[index3]);
                                if (index3 < parameter.ValidEquates.Count - 1)
                                    this._outputStream.Write("|");
                            }
                            this._outputStream.Write("]");
                        }

                        for (int index3 = 0; index3 < parameter.ShortHelp.Count; ++index3)
                        {
                            if (index3 > 0)
                            {
                                this._outputStream.Write("                      ");
                                for (int index4 = 0; index4 <= parameter.ParamText.Length; ++index4)
                                    this._outputStream.Write(" ");
                            }
                            this._outputStream.WriteLine(" " + parameter.ShortHelp[index3]);
                        }
                    }
                }

                this._outputStream.WriteLine("");
            }

            if (this.ExitCommand != null)
            {
                this._outputStream.Write("    " + this.ExitCommand.ConsoleEntry.ToUpper());
                if (this.ExitCommand.ShortHelp.Count > 0)
                {
                    for (int index = 1; index < 13 - this.ExitCommand.ConsoleEntry.Length; ++index)
                        this._outputStream.Write(" ");

                    this._outputStream.WriteLine(this.ExitCommand.ShortHelp[0]);
                    for (int index = 1; index < this.ExitCommand.ShortHelp.Count; ++index)
                        this._outputStream.WriteLine("                    " + this.ExitCommand.ShortHelp[index]);
                }
            }

            this._outputStream.WriteLine("\n    Usage: command parm1 = setting1 parm2 = setting2\n");
            return true;
        }

        private TextWriter _outputStream = Console.Out;
        /// <summary>
        /// All output is pumped through this object.  You can capture it by assigning your TextWriter here.
        /// </summary>
        public TextWriter OutputStream
        {
            get
            {
                return this._outputStream;
            }
            set
            {
                this._outputStream = value;
                Console.SetOut(value);
            }
        }

        private TextReader _inputStream = Console.In;
        /// <summary>
        /// The TextReader that accepts user input.  You can assign your own TextReader here to capture the user's entries.
        /// </summary>
        public TextReader InputStream
        {
            get
            {
                return this._inputStream;
            }
            set
            {
                this._inputStream = value;
                Console.SetIn(value);
            }
        }

        /// <summary>
        /// Restore the output stream to what it was.
        /// </summary>
        public void ResetOutputStream()
        {
            this._outputStream = this._originalOutputStream;
        }
    }
}
