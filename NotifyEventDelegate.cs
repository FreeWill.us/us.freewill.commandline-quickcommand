﻿// Copyright (c) 1998-2019 freewill.us
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.
// or IPFS hash QmbUetW94JtdSdVdATC4b9ASxCaiRn94AyNpFrc6twsLKy
namespace us.FreeWill
{
    /// <summary>
    /// Delegate for callback events.
    /// </summary>
    /// <param name="sender">When callback is called, the owning object reference is passed in.</param>
    /// <returns>Boolean value provided by the event handler.</returns>
    public delegate bool NotifyEventDelegate(object sender);
}
