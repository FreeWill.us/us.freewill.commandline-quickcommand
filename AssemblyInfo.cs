﻿using System.Runtime.InteropServices;
// Copyright (c) 1998-2019 freewill.us
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.
// or IPFS hash QmbUetW94JtdSdVdATC4b9ASxCaiRn94AyNpFrc6twsLKy
using System.Reflection;

[assembly: AssemblyCopyright("1998-2019")]
[assembly: AssemblyKeyName("")]
[assembly: AssemblyKeyFile("")]
[assembly: AssemblyDelaySign(false)]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyProduct("CommandLine QuickCommand")]
[assembly: AssemblyCompany("FreeWill.us")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyDescription("Library for automatic Command Line parsing and routing.")]
[assembly: AssemblyTitle("us.FreeWill.CommandLine")]
[assembly: AssemblyVersion("4.0.2.*")]
//[assembly: Guid("DD9FF2C2-D4A5-4B18-AE35-712B4868C6AB")]

