﻿// Copyright (c) 1998-2019 freewill.us
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.
// or IPFS hash QmbUetW94JtdSdVdATC4b9ASxCaiRn94AyNpFrc6twsLKy
using System;
using System.IO;
using System.Linq;
using us.FreeWill.CommandLine;

namespace CommandLine.Example.Console.Shell
{
    /// <summary>
    /// This is a very MINIMAL console app that replicates a very MINIMAL amount
    /// of functionality from the Windows Shell (a.k.a. "DOS BOX").  It exists
    /// to demonstrate the features of the CommandLine library.
    /// </summary>
    class Program
    {
        private static CommandLineType _cl;
        private static string _currentDir = Directory.GetCurrentDirectory();

        static void Main(string[] args)
        {
            _cl = new CommandLineType(_currentDir, ">");

            var dirCommand = new CommandType(DirCommand_CommandEventHandler, "dir", "Lists files in a directory.");
            _cl.AddCommand(dirCommand);

            var cdCommand = new CommandType(CDCommand_CommandEventHandler, "cd", "Change the directory.");
            //cdCommand takes no name=value parameters.  It will use the list of tokens supplied.  (See CDCommand_CommandEventHandeler).
            _cl.AddCommand(cdCommand);

            if (args.Length > 0)
                _cl.ExecDosCommand(args);
            else
                _cl.Execute();
        }


        #region Event Handlers...

        /// <summary>
        /// A rough simulation of the familiar DIR command.
        /// </summary>
        /// <param name="sender"></param>
        /// <returns></returns>
        private static bool DirCommand_CommandEventHandler(object sender)
        {
            var drives = DriveInfo.GetDrives();
            var thisDriveLetter = _currentDir.Substring(0, _currentDir.IndexOf(":")).ToUpper()[0];
            var thisDrive = drives.Where(d => d.Name == $"{thisDriveLetter}:\\").FirstOrDefault();

            _cl.OutputStream.WriteLine($" Volumn in drive {thisDriveLetter} is {thisDrive.VolumeLabel}");
            _cl.OutputStream.WriteLine($" Volume Serial Number is {GetDriveSerialNumber(thisDriveLetter)}");
            _cl.OutputStream.WriteLine($"\r\n Directory of {_currentDir}\r\n");

            var dirs = Directory.GetDirectories(_currentDir);
            foreach (var dir in dirs)
            {
                var parts = dir.Split('\\');
                var folder = parts[parts.Length - 1];
                var lastWriteTime = Directory.GetLastWriteTime(dir);
                _cl.OutputStream.WriteLine($"{FormatDate(lastWriteTime)}    <DIR>          {folder}");
            }

            var files = Directory.GetFiles(_currentDir);
            long fileSizeSum = 0;
            foreach (var file in files)
            {
                var lastWriteTime = File.GetLastWriteTime(file);
                var fi = new FileInfo(file);

                string sizeString = fi.Length.ToString().PadLeft(17, ' ');
                fileSizeSum += fi.Length;

                _cl.OutputStream.WriteLine($"{FormatDate(fi.LastWriteTime)} {sizeString} {Path.GetFileName(file)}");
            }

            _cl.OutputStream.WriteLine($"{files.Length.ToString().PadLeft(15, ' ')} File{((files.Length == 1) ? "" : "s")} {fileSizeSum.ToString().PadLeft(17, ' ')} byte{((fileSizeSum == 1) ? "" : "s")}");
            _cl.OutputStream.WriteLine($"{dirs.Length.ToString().PadLeft(15, ' ')} Dir{((dirs.Length == 1) ? "" : "s")}");

            return true;
        }


        /// <summary>
        /// Event handler to execute the CD command.
        /// </summary>
        /// <param name="sender">Reference to the CommandType object for the CD command.</param>
        /// <returns>True on sucessful execution.  False otherwise.</returns>
        /// <remarks>No paramter validation happened for this command because we provided no list of named paramters.  Instead, we'll look at the list of tokens provided and grab the first one to use as the new dir.</remarks>
        private static bool CDCommand_CommandEventHandler(object sender)
        {
            var command = (CommandType)sender;

            if (command.UserTokens == null || command.UserTokens.Count() == 0)
                return false;

            if (command.UserTokens[0] == "..") //User chose to go BACK one dir, so we'll chop off the last dir from the currend dir.
            {
                var parts = _currentDir.Split('\\').ToList();
                if (parts.Count() > 1)
                    parts.RemoveAt(parts.Count - 1);
                _currentDir = string.Join("\\", parts.ToArray());
                if (parts.Count() == 1)
                    _currentDir += '\\';
            }
            else //User entered a new subdir to go into.
            {
                var newDir = Path.Combine(_currentDir, command.UserTokens[0]);
                if (!Directory.Exists(newDir))
                {
                    _cl.OutputStream.WriteLine("The system cannot find the path specified.");
                    return false;
                }
                else
                    _currentDir = Path.Combine(_currentDir, command.UserTokens[0]);
            }
            _cl.Name   = _currentDir;
            _cl.Prompt = $"{_currentDir}>";
            return true;
        }

        /// <summary>
        /// Event handler to validate the CD command's "DIR" parameter.
        /// </summary>
        /// <param name="sender">Reference to the ParameterType object.</param>
        /// <returns>True on sucessful validation.  False otherwise.</returns>
        private static bool CDCommand_DirParm_ValidateFunction(object sender)
        {
            var parm = (ParameterType)sender;
            var exists = Directory.Exists(Path.Combine(_currentDir, parm.UserValue));
            if (!exists)
            {
                _cl.OutputStream.WriteLine("The system cannot find the path specified.");
                return false;
            }
            return true;
        }

        #endregion


        #region Utility Methods...

        private static string GetDriveSerialNumber(char driveLetter)
        {
            var disk = new System.Management.ManagementObject($"win32_logicaldisk.deviceid=\"{driveLetter}:\"");
            disk.Get();
            var SerialNumber = disk["VolumeSerialNumber"].ToString();
            return SerialNumber;
        }

        private static string FormatDate(DateTime dateTime)
        {
            return $"{dateTime.Day.ToString().PadLeft(2, '0')}-{dateTime.Month.ToString().PadLeft(2, '0')}-{dateTime.Year.ToString().PadLeft(4, '0')}  " +
                $"{dateTime.Hour.ToString().PadLeft(2, '0')}:{dateTime.Minute.ToString().PadLeft(2, '0')} {((dateTime.Hour > 11) ? "PM" : "AM")}";
        }

        #endregion
    }
}
