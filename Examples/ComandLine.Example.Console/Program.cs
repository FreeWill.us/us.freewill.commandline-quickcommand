﻿// Copyright (c) 1998-2019 freewill.us
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.
// or IPFS hash QmbUetW94JtdSdVdATC4b9ASxCaiRn94AyNpFrc6twsLKy
using us.FreeWill;
using us.FreeWill.CommandLine;

namespace ComandLine.Example.Console
{
    class Program
    {
        private static CommandLineType _cl;

        static void Main(string[] args)
        {
            //Instantiate the command line object, giving the initial display prompt.
            _cl = new CommandLineType("Example", ">");
            
            //Instantiate a command object, along with some simple help text and a reference to the command's callback function.
            var testCommand = new CommandType(
                commandEventHandler: TestCommand_CommandFunction, 
                consoleEntry       : "test", 
                shortHelpString    : "Just a sample command.");

            //Add a parameter that can be used.
            var parm = new ParameterType(
                eventHandler    : Parm_ValidateFunction, 
                parmLabel       : "SomeParm", 
                equationString  : "=", //What you expecte between the parm name and value (SomeParm = Hello).
                defaultUserValue: "Hello", 
                shortHelpString : "A sample command parameter that defaults to value \"Hello\".");

            //Add a parameter that can be used.
            var parm2 = new ParameterType(
                eventHandler    : Parm_ValidateFunction, 
                parmLabel       : "SomethingElse", 
                equationString  : "=", //What you expecte between the parm name and value (SomethingElse = World).
                defaultUserValue: "World", 
                shortHelpString : "A sample command parameter that defaults to value \"World\".");

            testCommand.ParameterList.Add(parm);
            testCommand.ParameterList.Add(parm2);

            //Add the command to the command line object so it can process it when users enter it.
            _cl.AddCommand(testCommand);
            
            //Tell it which of your commands exits the program (you can add extra functionality to the exit process here).
            //Comment out the next line and you'll get the custom exit command instead, along with its default help.
            _cl.SetExitCommand(new CommandType((NotifyEventDelegate)null, "Quit", "My custom exit command."));

            if (args.Length > 0)
            {
                //If launched from the OS console (or anywhere else) with parms passed in,
                //execute the one command and exit.
                _cl.ExecDosCommand(args);
            }
            else
            {
                DisplayWelcome(_cl);

                //Start up the command line.  It won't stop until someone enters the text you provided
                //  for the exit command OR if you provided no exit command, when the user types the
                //  text for the default exit command, which is "Exit".
                _cl.Execute();
            }
        }

        private static bool Parm_ValidateFunction(object sender)
        {
            var parm = (ParameterType) sender;
            _cl.OutputStream.WriteLine($"Parameter recognized \"{parm.ParamText}\" with value \"{parm.UserValue}\".");

            if (parm.UserValue.ToLower() == "fail")
                return false;

            return true;
        }

        /// <summary>
        /// The event handler for the Test command.  This will be called automatically when the user enters the "test" command.
        /// You write the code to handle the "Test" command here.
        /// </summary>
        /// <param name="sender">The sending CommandType object.</param>
        /// <returns>You return true or false depending on whether you determined this succeeded or failed.</returns>
        private static bool TestCommand_CommandFunction(object sender)
        {
            var command = (CommandType) sender;

            var cl = command.GetOwnerCommandLine();
            cl.OutputStream.WriteLine($"You entered [{command.ConsoleEntry}]");
            
            if (command.ConsoleEntry.Contains("fail"))
                return false;

            return true;
        }

        private static void DisplayWelcome(CommandLineType cl)
        {
            //Write a little intro for the users on startup...
            //Notice your CommandLineType object has an output stream.  You should always use this
            //instead of Console.WriteLine because you can change OutputStream to any stream you want.
            //CL defaults to the System.Console.Out TextWriter, but you can change it ot anything you want.

            cl.OutputStream.WriteLine("*******************************************************************");
            cl.OutputStream.WriteLine("*                                                                 *");
            cl.OutputStream.WriteLine("*  #     #           ##                                       ##  *");
            cl.OutputStream.WriteLine("*  #     #            #                                       ##  *");
            cl.OutputStream.WriteLine("*  #     #            #                                       ##  *");
            cl.OutputStream.WriteLine("*  #     #            #                                       ##  *");
            cl.OutputStream.WriteLine("*  #  #  #  #####     #     #####   #####  ### ##   #####     ##  *");
            cl.OutputStream.WriteLine("*  # ### # #     #    #    #     # #     # ## #  # #     #    ##  *");
            cl.OutputStream.WriteLine("*  ### ### #######    #    #       #     # #  #  # #######    ##  *");
            cl.OutputStream.WriteLine("*  ##   ## #          #    #     # #     # #  #  # #              *");
            cl.OutputStream.WriteLine("*  #     #  #####    ###    #####   #####  #     #  #####     ##  *");
            cl.OutputStream.WriteLine("*                                                                 *");
            cl.OutputStream.WriteLine("*******************************************************************\r\n");

            cl.OutputStream.WriteLine("Welcome to the CommandLine sample Console application.");
            cl.OutputStream.WriteLine("Type \"Help\".");
        }

    }
}
