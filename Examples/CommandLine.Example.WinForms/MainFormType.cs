﻿// Copyright (c) 1998-2019 freewill.us
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.
// or IPFS hash QmbUetW94JtdSdVdATC4b9ASxCaiRn94AyNpFrc6twsLKy
using System;
using System.IO;
using System.Windows.Forms;
using CL = us.FreeWill.CommandLine;

namespace CommandLine.Example.WinForms
{
    public partial class MainFormType : Form
    {
        /// <summary>
        /// Instantiate the CommandLineType object and initialize it with a prompt.
        /// </summary>
        private CL.CommandLineType _commandLine = new CL.CommandLineType("Example", ">");

        public MainFormType()
        {
            InitializeComponent();
        }

        private void File_NewConsoleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Create the Console window, passing it the command line object.
            //That's where the user will interact with the command line.
            var console = new ConsoleFormType(this._commandLine);
            console.Owner = this;
            console.MdiParent = this;
            console.Show();
        }


        private void MainFormType_Shown(object sender, EventArgs e)
        {
            //Add a sample command to the command line object and point it to its event handler and give it some help text.
            var testCommand = new CL.CommandType(TestCommand_CommandFunction, "Test", "Sample command");
            this._commandLine.AddCommand(testCommand);

            //Since we're not using the Console, we need to trap the output so we can do something with it.
            //A StringWriter will work just fine for this purpose.
            //Our ConsoleFormType window can capture the output and display it however it likes.
            this._commandLine.OutputStream = new StringWriter();

            //Notice there's no exit command?  That's not needed in a winforms app.
            //Whatever control is passing the input to the command line object can check for an exit command (if it wants to).
            //Also notice that we're NOT calling this._commandLine.Execute()?  Again, thats because we're NOT using an actual
            //Console app.  We're handling keyboard input from a standard TextBox control and CALLING CommandLineType.ExecCommand()
            //and passing it the text from our input textbox, so there's no concept of "exiting" the CommandLineType object.
        }

        /// <summary>
        /// Event handler for the "test" command object.  This example purposely writes it here, in the MainForm,
        /// rather than the ConsoleForm so that you can see that it doesn't need to know anything about the ConsoleForm
        /// to output text to it.  It simply writes to the _commandLine.OutputStream.  The ConsoleForm will capture any
        /// text written to that stream and display it however it likes.
        /// </summary>
        /// <param name="sender">The command object that owns the comment that was processed.</param>
        /// <returns></returns>
        private bool TestCommand_CommandFunction(object sender)
        {
            var command = (CL.CommandType) sender;

            //Write to the command line's output.  It will be handled by the console form.
            _commandLine.OutputStream.WriteLine($"Test command executed [{command.ConsoleEntry}] and handled in the MainForm.");
            MessageBox.Show("This dialog box is being created from the MainForm and the command line is responding by doing something Windowy.  Command event handlers can do anything, not just command line like stuff.", $"You used the {command.ConsoleEntry} command.");

            return true;
        }
    }
}
