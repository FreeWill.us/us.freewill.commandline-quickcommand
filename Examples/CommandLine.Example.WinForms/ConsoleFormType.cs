﻿// Copyright (c) 1998-2019 freewill.us
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.
// or IPFS hash QmbUetW94JtdSdVdATC4b9ASxCaiRn94AyNpFrc6twsLKy
using System.IO;
using System.Windows.Forms;
using us.FreeWill.CommandLine;

namespace CommandLine.Example.WinForms
{
    public partial class ConsoleFormType : Form
    {
        private readonly CommandLineType _commandLine;

        public ConsoleFormType(CommandLineType commandLine)
        {
            this._commandLine = commandLine;
            InitializeComponent();
        }

        private void CommandTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Check if user hit the enter key.  Handle ANY command here.
            //All we're doing is asking the command line to execute the
            //command, capturing the output, and checking if they want to exit.
            //We're NOT processing the commands.  That's the job of the CommandLineType.ExecCommand().
            if ((int) e.KeyChar == (int) us.FreeWill.SpecialKeysEnum.CR)
            {
                //Send the user entered command to the command line's ExecCommand() method to be processed.
                var commandResult = this._commandLine.ExecCommand(this.CommandTextBox.Text);

                //Check if the user entered whatever command triggered the exit event.
                if (commandResult == CommandResultEnum.Exit)
                    this.Close(); //close this window if the user entered the exit command.

                //Capture the command's ouput and display in the OutputTextBox.
                this.OutputTextBox.Text += this._commandLine.OutputStream.ToString();

                //Reassign the OutputStream to a new StringWriter() (old one will not let go of old output).
                this._commandLine.OutputStream = new StringWriter();

                //Show whether or not the command was successful.
                this.OutputTextBox.Text += $"\r\rResult = {commandResult}.\r\n\r\n";

                //Clear the input box.
                ((TextBox)sender).Text = string.Empty;

                //Scroll to the bottom so we can see what was just added:
                this.OutputTextBox.SelectionStart = this.OutputTextBox.Text.Length;
                this.OutputTextBox.ScrollToCaret();
            }
        }
    }
}
