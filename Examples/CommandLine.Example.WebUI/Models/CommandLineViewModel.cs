﻿// Copyright (c) 1998-2019 freewill.us
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.
// or IPFS hash QmbUetW94JtdSdVdATC4b9ASxCaiRn94AyNpFrc6twsLKy

namespace CommandLine.Example.WebUI.Models
{
    public class CommandLineViewModel
    {
        public string ConsoleEntry { get; set; }
        public string Output { get; set; } = string.Empty;
    }
}