﻿// Copyright (c) 1998-2019 freewill.us
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.
// or IPFS hash QmbUetW94JtdSdVdATC4b9ASxCaiRn94AyNpFrc6twsLKy
using System;
using System.IO;
using System.Web.Mvc;
using CommandLine.Example.WebUI.Models;
using CL = us.FreeWill.CommandLine;

namespace CommandLine.Example.WebUI.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "CommandLine Web UI example.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "CommandLine Contact Info:";

            return View();
        }

        public ActionResult CommandLine()
        {
            ViewBag.Message = "Command Line";

            return View();
        }

        /// <summary>
        /// Instantiate the CommandLineType object and initialize it with a prompt.
        /// </summary>
        private CL.CommandLineType _commandLine = new CL.CommandLineType("Example", ">");

        [HttpPost]
        public ActionResult CommandLine(CommandLineViewModel model)
        {
            //The code below was copied from the WinForms sample project, with minor modifications...

            //Add a sample command to the command line object and point it to its event handler and give it some help text.
            var testCommand = new CL.CommandType(TestCommand_CommandFunction, "Test", "Sample command");
            this._commandLine.AddCommand(testCommand);

            //A date command with help and a format paramter object.
            var dateCommand = new CL.CommandType(DateCommand_CommandEventHandler, "Date", "Gets the current date.");
            var dateFormatParm = new CL.ParameterType(null, "Format", "=", "f", "Sets the format of the date display");
            dateFormatParm.ShortHelp.Add("https://docs.microsoft.com/en-us/dotnet/standard/base-types/custom-date-and-time-format-strings");
            dateCommand.ParameterList.Add(dateFormatParm);
            this._commandLine.AddCommand(dateCommand);

            //Since we're not using the Console, we need to trap the output so we can do something with it.
            //A StringWriter will work just fine for this purpose.
            //We'll capture the output via the _commandLine.OutputStream and put it in the ViewModel.
            this._commandLine.OutputStream = new StringWriter();

            //Notice there's no exit command?  That's not needed in a web UI app.
            //Whatever control is passing the input to the command line object can check for an exit command (if it wants to).
            //Also notice that we're NOT calling this._commandLine.Execute()?  Again, thats because we're NOT using an actual
            //Console app.  We're handling keyboard input from a standard @Html.TextBoxFor() control from the view and CALLING
            //CommandLineType.ExecCommand() here and passing it the text from our input textbox, so there's no concept of
            //"exiting" the CommandLineType object.  However, we're going to respond to "exit" anyway and redirect them
            //to the Index action method to send them back to the home page.

            //Send the user entered command to the command line's ExecCommand() method to be processed.
            var commandResult = this._commandLine.ExecCommand(model.ConsoleEntry);

            //Check if the user entered whatever command triggered the exit event.
            switch (commandResult)
            {
                case CL.CommandResultEnum.Fail:
                    model.Output += "Command not recognized.";
                    break;
                case CL.CommandResultEnum.Exit:
                    return RedirectToAction("Index", "Home");
            }

            //Capture the command's ouput and assign to the view model.
            model.Output += this._commandLine.OutputStream.ToString(); //.Replace(" ", "&nbsp;"); //.Replace(Environment.NewLine, @"<br />"); ;

            //Reassign the OutputStream to a new StringWriter() (old one will not let go of old output).
            this._commandLine.OutputStream = new StringWriter();

            return View(model);
        }

        private bool DateCommand_CommandEventHandler(object sender)
        {
            var command = (CL.CommandType)sender;

            var parm = command.GetParameterByName("Format"); //TODO: event handler not firing if user enters parameters.

            string display = "Unrecognized format specifier";
            try
            {
                display = DateTime.Now.ToString(parm.UserValue);
            }
            catch (Exception ex)
            {
                display = ex.Message;

            }

            _commandLine.OutputStream.WriteLine($"{display}");

            return true;
        }

        /// <summary>
        /// Event handler for the "test" command object.  It simply writes to the _commandLine.OutputStream.
        /// The controller action will capture any text written to that stream and display it however it likes.
        /// </summary>
        /// <param name="sender">The command object that owns the comment that was processed.</param>
        /// <returns>True for success, false for failure.</returns>
        private bool TestCommand_CommandFunction(object sender)
        {
            var command = (CL.CommandType)sender;

            //Write to the command line's output.
            _commandLine.OutputStream.WriteLine($"Test command executed [{command.ConsoleEntry}].");

            return true;
        }
    }
}